import React from 'react'
import {
    Container,
    // Dropdown,
    Image,
    Menu,
} from 'semantic-ui-react'
import Link from 'next/link'


export default function TopMenu(props) {
    return (
        <Menu fixed='top' inverted>
            <Container>
                    <Menu.Item header>
                        <Image size='mini' src='/static/logo.jpg' style={{ marginRight: '1.5em' }} />
                        <Link href="/">
                            <a>Bastion Siege v2 Info</a>
                        </Link>

                    </Menu.Item>
                <Menu.Item>
                    <Link href="/">
                        <a>Главная</a>
                    </Link>
                </Menu.Item>
                {/*<Menu.Item>Калькулятор боя</Menu.Item>*/}
                <Menu.Item>F.A.Q.</Menu.Item>
                {/*<Menu.Item>Полезные ссылки</Menu.Item>*/}

                {/*<Dropdown item simple text='Dropdown'>*/}
                    {/*<Dropdown.Menu>*/}
                        {/*<Dropdown.Item>List Item</Dropdown.Item>*/}
                        {/*<Dropdown.Item>List Item</Dropdown.Item>*/}
                        {/*<Dropdown.Divider />*/}
                        {/*<Dropdown.Header>Header Item</Dropdown.Header>*/}
                        {/*<Dropdown.Item>*/}
                            {/*<i className='dropdown icon' />*/}
                            {/*<span className='text'>Submenu</span>*/}
                            {/*<Dropdown.Menu>*/}
                                {/*<Dropdown.Item>List Item</Dropdown.Item>*/}
                                {/*<Dropdown.Item>List Item</Dropdown.Item>*/}
                            {/*</Dropdown.Menu>*/}
                        {/*</Dropdown.Item>*/}
                        {/*<Dropdown.Item>List Item</Dropdown.Item>*/}
                    {/*</Dropdown.Menu>*/}
                {/*</Dropdown>*/}
            </Container>
        </Menu>
    )
}