import ResourceTable from './ResourceTable';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {setResource} from '../../actions/resources';



const mapStateToProps = ({resource}) => resource;
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ setResource }, dispatch);
};


export default connect(mapStateToProps, mapDispatchToProps)(ResourceTable)

