import React from 'react';
import {Input} from "semantic-ui-react";
import {setResource} from "../../actions/resources";

export default class ResourceTable extends React.Component{
    render() {
        return (
            <React.Fragment>
                <div>
                <Input
                    onChange={(event) => this.props.setResource('gold', event.target.value)}
                    type="number"
                    size="mini"
                    value={this.props.gold}
                    label={{ basic: true, content: 'Золото' }}
                    labelPosition='left'
                    placeholder='Золото &nbsp;'
                />
                </div>
                <div>
                <Input
                    onChange={(event) => this.props.setResource('wood', event.target.value)}
                    type="number"
                    size="mini"
                    value={this.props.wood}
                    label={{ basic: true, content: 'Дерево' }}
                    labelPosition='left'
                    placeholder='Дерево'
                />
                </div>
                <div>
                <Input
                    onChange={(event) => this.props.setResource('stone', event.target.value)}
                    type="number"
                    size="mini"
                    value={this.props.stone}
                    label={{ basic: true, content: 'Камень' }}
                    labelPosition='left'
                    placeholder='Камень'
                />
                </div>
            </React.Fragment>
        )
    }
}