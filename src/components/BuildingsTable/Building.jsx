import React from 'react'
import {Input, Table} from "semantic-ui-react";
import PropTypes from 'prop-types';
import {Twemoji,} from 'react-emoji-render';
import moment from 'moment'
moment.locale('ru')


export default class Building extends React.Component {
    static propTypes = {
        name: PropTypes.string,
        alias: PropTypes.string.isRequired,
        current: PropTypes.number.isRequired,
        target: PropTypes.number.isRequired,
        gold: PropTypes.number.isRequired,
        wood: PropTypes.number.isRequired,
        stone: PropTypes.number.isRequired,
        buildings: PropTypes.any // todo add shape
    };


    trade() {
        const h = this.props.buildings.house.current * 5;
        const tt = Math.floor(this.props.buildings.tower.current / 10 )* 10;
        const woodIncome = this.props.buildings.wood.current * 2;
        const stoneIncome = this.props.buildings.stone.current * 2;
        const goldInclome  = h + tt;

        const {current: c, target: t} = this.props;
        let goldValue = this._calc(c, t,  this.props.gold) - this.props.resource.gold
        let woodValue = this._calc(c, t,  this.props.wood)- this.props.resource.wood
        let stoneValue = this._calc(c, t,  this.props.stone)- this.props.resource.stone

        const operations = {
            wood: 0,
            stone: 0
        };

        while(goldValue > 0 || woodValue > 0 || stoneValue > 0) {
            let freeMoney = 0;
            if (goldValue <= 0) {
                freeMoney += goldInclome
            } else {
                goldValue -= goldInclome
            }
            if (woodValue <= 0) {
                freeMoney += woodIncome * 0.7;
                operations.wood -= woodIncome;
            } else {
                woodValue -= woodIncome;
            }
            if (stoneValue <= 0) {
                freeMoney += stoneIncome * 0.7;
                operations.stone -= stoneIncome;
            } else {
                stoneValue -= stoneIncome;
            }

            const goldTime = goldValue / goldInclome;
            const woodTime = woodValue / woodIncome;
            const stoneTime = stoneValue / stoneIncome;

            if (goldTime > woodTime && goldTime > stoneTime) {
                // ...
            } else if (woodTime > stoneTime) {
                const buyWood = freeMoney / 1.3;
                woodValue -= buyWood;
                operations.wood += buyWood;
            } else {
                const buyStone = freeMoney / 1.3;
                stoneValue -= buyStone;
                operations.stone += buyStone;
            }

        }

        const fix = (x) => Number(Math.round(x)).toLocaleString();

        return (
            <React.Fragment>
                { operations.wood !==0 && <div><Twemoji text={`:evergreen_tree: Дерево: ${fix(operations.wood)}`}/></div> }
                { operations.stone !==0 && <div>{`Камень: ${fix(operations.stone)}`}</div> }
            </React.Fragment>
        );
    }


    _calc(c, t, coeff) {
        return Math.floor(
            (
                (
                    Math.pow(t - 1, 3) - Math.pow(c - 1, 3)
                ) / 3 +
                (
                    Math.pow(t - 1, 2) - Math.pow(c - 1, 2)
                ) / 2 +
                (t - c) / 6
            ) * coeff * 2.5 / 4
        );
    }

    calc(key, title) {
        const {current: c, target: t} = this.props;

        const value = this._calc(c, t, this.props[key]);

        return (
            <div style={{display: 'flex'}}>
                <div style={{flex: '50%'}}>{title}</div>
                <div style={{flex: '50%'}}>{Number(value).toLocaleString()}</div>
            </div>
        )
    }

    resolveBonus(alias) {
        const {current: c, target: t} = this.props;
        switch (alias) {
            case 'tower':
                return Math.floor(t/10)* 10 - Math.floor(c/10 )* 10;
            case 'house':
                return (t-c);
            case 'food':
                const prev = Math.floor(c/10 )* 10 + c - this.props.buildings.house.current * 2.5;
                const next = Math.floor(t/10 )* 10 + t - this.props.buildings.house.target * 2.5;
                return next - prev;
            case 'wood':
            case 'stone':
                return Math.floor(t/10)* 10 - Math.floor(c/10 )* 10 + t - c;
            case 'horse':
                return t - c;
            default:
                return void(0);
        }
    }

    resolveCost() {
        const {current: c, target: t} = this.props;

        const market = this._calc(c, t, this.props.wood) * 1.3 + this._calc(c, t, this.props.stone) * 1.3 + this._calc(c, t, this.props.gold);
        const self = this._calc(c, t, this.props.wood) + this._calc(c, t, this.props.stone) + this._calc(c, t, this.props.gold);

        return `${Number(self   ).toLocaleString()} / ${Number(market).toLocaleString()}`
    }

    resolveNeedTower() {
        const {target: t} = this.props;
        const c = t - 1;
        return Math.ceil(Math.sqrt(this._calc(c, t, this.props.gold) / 100))
    }

    resolveNeedStorage() {
        const {target: t} = this.props;
        const c = t - 1;

        return Math.ceil(Math.sqrt(this._calc(c, t, Math.max(this.props.stone, this.props.wood)) / 50))
    }

    resolveIncome(alias) {
        const {current: c, target: t} = this.props;
        switch (alias) {
            case 'tower':
                return Math.floor(c/10 )* 10;
            case 'house':
                return c * 5;
            case 'food':
                return Math.floor(c/10 )* 10 + c - this.props.buildings.house.current * 2.5;
            case 'wood':
            case 'stone':
                return Math.floor(c/10 )* 10 + c;
            case 'horse':
                return t - c;
            default:
                return void(0);
        }
    }

    timeDiff() {
        const {current: c, target: t} = this.props;

        const gold = this._calc(c, t, this.props.gold);
        const wood = this._calc(c, t, this.props.wood);
        const stone = this._calc(c, t, this.props.stone);

        const h = this.props.buildings.house.current * 5;
        const tt = Math.floor(this.props.buildings.tower.current / 10 )* 10;
        const w = this.props.buildings.wood.current * 2;
        const s = this.props.buildings.stone.current * 2;


        const minutes = (gold + wood + stone) / (h + tt + w+ s);

        const date = moment().add( minutes, "minutes");

        const dM = date.diff(moment(), 'minutes', true);
        if (dM < 100) {
            return `${Number(dM).toFixed(2)} минут`
        }

        const dH = date.diff(moment(), 'hours', true);
        if (dH < 50) {
            return `${Number(dH).toFixed(2)} часов`
        }
        const dD = date.diff(moment(), 'days', true);
        return `${Number(dD).toFixed(2)} дней`
    }

    render() {
        return (
            <Table.Row>
                <Table.Cell collapsing>
                    <Twemoji text={`:${this.props.icon}:`}/> {this.props.title}
                </Table.Cell>
                <Table.Cell width={2} collapsing>
                    <Input
                        size='mini'
                        value={this.props.current}
                        type="number"
                        onChange={({target}) => this.props.onChange('current', +target.value)}
                    />
                </Table.Cell>
                <Table.Cell width={2} collapsing>
                    <Input
                        size='mini'
                        value={this.props.target}
                        type="number"
                        min={this.props.current}
                        max={500}
                        error={this.props.current > this.props.target}
                        onChange={({target}) => this.props.onChange('target', +target.value)}
                    />
                </Table.Cell>
                <Table.Cell collapsing textAlign='right'>
                    {
                        Number.isFinite(this.resolveIncome(this.props.alias))
                            ? <Twemoji text={`${this.resolveIncome(this.props.alias)} :moneybag:`} />
                            : ''
                    }
                </Table.Cell>
                <Table.Cell collapsing textAlign='right'>
                    {
                        Number.isFinite(this.resolveBonus(this.props.alias))
                            ? <Twemoji text={`${this.resolveBonus(this.props.alias)} :moneybag:`} />
                            : ''
                    }
                </Table.Cell>
                <Table.Cell width={2}>
                    {this.calc('gold', <Twemoji text={':moneybag: Золото'}/>)}
                    {this.calc('wood', <Twemoji text={':evergreen_tree: Дерево'}/>)}
                    {this.calc('stone', 'Камень')}
                </Table.Cell>
                <Table.Cell>{this.timeDiff()}</Table.Cell>
                <Table.Cell>{this.resolveCost()} <Twemoji text={':moneybag:'}/></Table.Cell>
                <Table.Cell>
                    <div>Ратуша: {this.resolveNeedTower()}</div>
                    <div>Склад: {this.resolveNeedStorage()}</div>
                </Table.Cell>
                <Table.Cell>
                    {this.trade()}
                    {/*<div><Twemoji text={':evergreen_tree: Дерево: + 2,300'}/></div>*/}
                    {/*<div><Twemoji text={'Камень - 1,258'}/></div>*/}
                </Table.Cell>
            </Table.Row>
        )
    }
}