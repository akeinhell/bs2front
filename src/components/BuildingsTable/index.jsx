import BuildingsTable from './BuildingsTable';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {setValue, restoreValues} from '../../actions/buildings';



const mapStateToProps = ({buildings, resource}) => ({
    ...buildings,
    resource
});
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ setValue, restoreValues }, dispatch);
};


export default connect(mapStateToProps, mapDispatchToProps)(BuildingsTable)

