import React from 'react'
import {Icon, Table} from 'semantic-ui-react'
import Building from './Building';

export default class BuildingsTable extends React.Component {

    componentDidMount() {
        this.props.restoreValues()
    }

    render() {
        return (
            <Table celled striped>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell colSpan='10'>Расчет стоимости построек</Table.HeaderCell>
                    </Table.Row>
                    <Table.Row>
                        <Table.HeaderCell>Постройка</Table.HeaderCell>
                        <Table.HeaderCell>Уровень</Table.HeaderCell>
                        <Table.HeaderCell>планируемый</Table.HeaderCell>
                        <Table.HeaderCell>Доходность</Table.HeaderCell>
                        <Table.HeaderCell>Прирост дохода</Table.HeaderCell>
                        <Table.HeaderCell>Стоимость апа</Table.HeaderCell>
                        <Table.HeaderCell>Время до улучшения</Table.HeaderCell>
                        <Table.HeaderCell>Стоимость</Table.HeaderCell>
                        <Table.HeaderCell>Требования</Table.HeaderCell>
                        <Table.HeaderCell>Операции на рынке</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {
                        this.props.show
                            .map(alias => (
                                <Building
                                    key={`building_${alias}`}
                                    onChange={(key, value) => {
                                        this.props.setValue(alias, key, value)
                                    }}
                                    {...this.props[alias]}
                                    buildings={this.props}
                                    resource={this.props.resource}
                                    alias={alias}
                                >
                                    key: {alias}
                                </Building>
                            ))

                    }
                </Table.Body>
            </Table>
        )
    }
}