import React from 'react'
import {
    Container,
} from 'semantic-ui-react'
import TopMenu from './TopMenu';
import Head from 'next/head'
import { YMInitializer } from 'react-yandex-metrika';


class Layout extends React.Component {
    render() {
        const props = this.props;
        return (
            <React.Fragment>
                <Head>
                    <title>Bs2 tools</title>
                    <link
                        rel="stylesheet"
                        href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.1/dist/semantic.min.css"
                    />
                    <link rel="shortcut icon" href="/static/favicon.ico" type="image/x-icon"/>
                    <link rel="icon" href="/static/favicon.ico" type="image/x-icon"/>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                </Head>
                <YMInitializer accounts={[52078542]} />
                <TopMenu/>
                <Container fluid style={{ marginTop: '7em', paddingLeft: '3em',  paddingRight: '3em', }}>
                    {props.children}
                </Container>


            </React.Fragment>
        )
    }
}


export default Layout