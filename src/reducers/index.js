import { combineReducers } from 'redux';
import buildings from './buildings';
import resource from './resource';

export default combineReducers({
    buildings,
    resource,
});
