import {SET_VALUE} from "../actions/buildings";

const defaultState = {
    house: {
        target: 2,
        title: 'Дома',
        icon: 'house_with_garden',
        current: 2,
        gold: 30,
        wood: 28,
        stone: 20,
    },
    tower: {
        target: 2,
        title: 'ратуша',
        icon: 'post_office',
        current: 2,
        gold: 70,
        wood: 20,
        stone: 25,
    },
    storage: {
        target: 2,
        title: 'склад',
        icon: 'house',
        current: 2,
        gold: 15,
        wood: 6,
        stone: 5,
    },
    food: {
        target: 2,
        title: 'ферма',
        icon: 'ear_of_rice',
        current: 2,
        gold: 10,
        wood: 4,
        stone: 5,
    },
    wood: {
        target: 2,
        title: 'лесопилка',
        icon: 'evergreen_tree',
        current: 2,
        gold: 25,
        wood: 3,
        stone: 10,
    },
    stone: {
        target: 2,
        title: 'шахта',
        icon: 'poop',
        current: 2,
        gold: 35,
        wood: 11,
        stone: 3,
    },
    barrack: {
        target: 2,
        title: 'казарма',
        icon: 'poop',
        current: 2,
        gold: 55,
        wood: 20,
        stone: 25,
    },
    horse: {
        target: 2,
        title: 'конюшня',
        icon: 'horse',
        current: 2,
        gold: 50,
        wood: 15,
        stone: 5,
    },
    univer: {
        target: 2,
        title: 'университет',
        icon: 'mortar_board',
        current: 2,
        gold: 50,
        wood: 27,
        stone: 30,
    },
    show: [
        'house',
        'tower',
        'storage',
        'food',
        'wood',
        'stone',
        'barrack',
        'horse',
        'univer',
    ]
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case SET_VALUE:{
            const {building, key, value} = action.payload;

            return {
                ...state,
                [building]: {
                    ...state[building],
                    [key]: value
                }
            };
        }
        default:
            return state;
    }
};
