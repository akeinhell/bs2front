import {SET_RESOURCE} from "../actions/resources";

const defaultState = {
    gold: 0,
    wood: 0,
    stone: 0,
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case SET_RESOURCE:{
            const {key, value} = action.payload;

            return {
                ...state,
                [key]: value
            };
        }
        default:
            return state;
    }
};
