export const SET_RESOURCE = 'SET_RESOURCE';

export const setResource = (key, value) => ({type: SET_RESOURCE, payload:{key, value: +value}});