export const SET_VALUE = 'SET_VALUE';
const storeLocalstorage = () => (dispatch, getState) => {
    const {buildings} = getState();
    const data = Object.keys(buildings)
        .filter(key => key !== 'show')
        .reduce((acc, key) => {
            return {
                ...acc,
                [key]: [buildings[key].current || 1 , buildings[key].target || 2]
            }
        }, {});
    localStorage.setItem('buildings',JSON.stringify(data))
};
export const setValue = (building, key, value) => async dispatch => {
    await dispatch(({
        type: SET_VALUE,
        payload: {building, key, value}
    }));
    dispatch(storeLocalstorage())
};

export const restoreValues = () => dispatch => {
    try {
       let values = JSON.parse(localStorage.getItem('buildings'));
       for( const [buildingName, value] of Object.entries(values)) {
           const [c, v] = value;
           c && dispatch(setValue(buildingName, 'current', c));
           v && dispatch(setValue(buildingName, 'target', v));
       }
    }  catch (e) {
        console.warn('fail restore', e.message);
    }
};