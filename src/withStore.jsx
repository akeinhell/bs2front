import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from './reducers';
import thunkMiddleware from 'redux-thunk';

const isServer = typeof window === 'undefined';
const __NEXT_REDUX_STORE__ = '__NEXT_REDUX_STORE__';

function getOrCreateStore(initialState) {
    if (isServer) {
        return initializeStore(initialState);
    }

    if (!window[__NEXT_REDUX_STORE__]) {
        window[__NEXT_REDUX_STORE__] = initializeStore(initialState);
    }
    return window[__NEXT_REDUX_STORE__];
}

const m = [
    thunkMiddleware,
    // !isServer && window.__REDUX_DEVTOOLS_EXTENSION__
].filter(Boolean)


const middleware = applyMiddleware(...m);

export function initializeStore(initialState = {}) {
    return createStore(
        reducers,
        initialState,
        middleware
    );
}

export default App => {
    return class AppWithRedux extends React.Component {
        static async getInitialProps(appContext) {
            const reduxStore = getOrCreateStore();

            appContext.ctx.reduxStore = reduxStore;

            let appProps = {};
            if (typeof App.getInitialProps === 'function') {
                appProps = await App.getInitialProps(appContext);
            }

            return {
                ...appProps,
                initialReduxState: reduxStore.getState(),
            };
        }

        constructor(props) {
            super(props);
            this.reduxStore = getOrCreateStore(props.initialReduxState);
        }

        render() {
            return <App {...this.props} reduxStore={this.reduxStore} />;
        }
    };
};