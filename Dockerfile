FROM node:11-alpine

EXPOSE 80
WORKDIR /app
ADD ./package*.json ./

RUN npm install

ADD ./ /app
RUN npm run build

CMD ./node_modules/.bin/next start -H 0.0.0.0 -p 80