import App, {Container} from 'next/app'
import React from 'react'
import withReduxStore from '../src/withStore'
import { Provider } from 'react-redux'

class BsApp extends App {
    render () {
        const {Component, pageProps, reduxStore} = this.props
        return (
            <Container>
                <Provider store={reduxStore}>
                    <Component {...pageProps} />
                </Provider>
            </Container>
        )
    }
}

export default withReduxStore(BsApp)