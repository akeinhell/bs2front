import {Container, Grid, Header, Image} from "semantic-ui-react";
import React from "react";
import BuildingsTable from '../src/components/BuildingsTable';
import Layout from '../src/components/Layout';
import ResourceTable from "../src/components/ResourceTable";
import University from "../src/components/University/University";

export default function IndexPage() {
    return (
        <Layout>
            <Header as='h1'>Bs2 Calc</Header>
            <p>
                <a href="https://t.me/bs2info">Новости проекта</a>
            </p>
            <p>
                Бот в телеграмме: <a href="https://t.me/bs2infobot">@bs2infobot</a>
            </p>
            <Grid columns={2} divided>
                <Grid.Row>
                    <Grid.Column>
                        <ResourceTable/>
                    </Grid.Column>
                    <Grid.Column>
                        <University/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            <BuildingsTable/>
        </Layout>
    )
}